include("rmsd.jl")
include("../../mds_julia/mds_metric.jl")
using PyPlot

read_data(filename) = float64(readdlm(filename)[2:end,:])

function load_mcmc_filenames(num_files::Int; postfix="double")
    mcmc_filenames = Array(String, num_files)
    mcmc_labels = Array(String, num_files)
    for i=1:num_files
        filename = string("../mcmc_", string(i), postfix, ".txt")
        label = string(i)
        mcmc_filenames[i] = filename
        mcmc_labels[i] = string(postfix[1:2], label)
    end
    return mcmc_filenames, mcmc_labels
end

filenames = ["../molecular_simulations/poly1/block0.dat",
             "../molecular_simulations/poly2/block0.dat",
             "mds_4double.txt",
             "mds_4single.txt",
            ]
labels = ["poly1",
          "poly2",
          "mds_both",
          "mds_1",
         ]
 
#filenames_mcmc, labels_mcmc = load_mcmc_filenames(100, postfix="3single")
#filenames_mcmc_2, labels_mcmc_2 = load_mcmc_filenames(20, postfix="2single")
#filenames = cat(1, filenames, filenames_mcmc, filenames_mcmc_2)
#labels = cat(1, labels, labels_mcmc, labels_mcmc_2)
function main()
    filenames_new = filenames
    distances = zeros(length(filenames_new), length(filenames_new))
    for (i1, f1) in enumerate(filenames_new)
        for (i2, f2) in enumerate(filenames_new[i1+1:end])
            i2 = i2+i1
            data1 = read_data(f1)
            data2 = read_data(f2)
            dist1, R, s, t = rmsd_horn(data1, data2)
            dist2, R, s, t = rmsd_horn(data2, data1)
            distances[i1, i2] = (dist1 + dist2)/2
            distances[i2, i1] = (dist1 + dist2)/2
        end
    end
    #positions = mds_metric(distances, 2)
    return distances
end
distances = main()
points_3 = mds_metric(distances, dims=3)
points = mds_metric(distances, dims=2)
PyPlot.scatter(points[:,1], points[:,2])
for i in 1:size(points, 1)
    p = points[i,:]
    PyPlot.annotate(labels[i], (p[1], p[2]))
end
